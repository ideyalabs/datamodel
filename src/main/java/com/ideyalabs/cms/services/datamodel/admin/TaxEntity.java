/**
 * Babu Gali
 *
 * May 22, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "TAX")
@SequenceGenerator(name = "_SEQ_GEN", sequenceName = "TAX_SEQ", initialValue = 1)
public class TaxEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "TAX_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID" )
	private Long id;
	
	@Column( name ="COUNTRY" )
	private String country;

	@Column( name = "CORPORATE_TAX" )
	private float corporateTax;
	
	@Column( name = "PRIVATE_TAX" )
	private float privateTax;
}
