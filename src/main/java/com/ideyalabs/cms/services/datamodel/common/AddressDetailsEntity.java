/**
 * Babu Gali
 *
 * May 22, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ADDRESS_DETAILS")
@SequenceGenerator(name = "ADDRESS_DETAILS_SEQ_GEN", sequenceName = "ADDRESS_DETAILS_SEQ", initialValue = 1)
public class AddressDetailsEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "ADDRESS_DETAILS_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	@Column(name = "HOUSENO")
	private String hoseNo;

	@Column(name = "STREET")
	private String street;
	
	@Column(name = "ZIP_CODE")
	private String zipCode;
	
	@ManyToOne
	@JoinColumn(name = "CITY_ID")
	private LocationEntity city;
	
	@ManyToOne
	@JoinColumn(name = "STATE_ID")
	private LocationEntity state;
	
	@ManyToOne
	@JoinColumn(name = "COUNTRY_ID")
	private LocationEntity country;

}
