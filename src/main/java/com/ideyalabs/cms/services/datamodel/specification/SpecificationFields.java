package com.ideyalabs.cms.services.datamodel.specification;

import lombok.Getter;
import lombok.Setter;

/**
 * May 10, 2020
 * 
 * Babu Gali
 */

@Getter
@Setter
public class SpecificationFields {

	private String columName;
	private String dataType;
	private Object value;
	private String conditionType;
	private String joinCoulnName;

}
