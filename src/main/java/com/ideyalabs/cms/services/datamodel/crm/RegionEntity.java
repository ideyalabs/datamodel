
package com.ideyalabs.cms.services.datamodel.crm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * May 16, 2020
 * Babu Gali
 */

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "region")
@SequenceGenerator(name = "region_seq_gen", sequenceName = "region_seq", initialValue = 1)
public class RegionEntity extends AbstractAuditEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2346189659484385017L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "region_seq_gen", strategy = GenerationType.SEQUENCE)
	private Long ID;
	
	@Column(name = "COUNTRY_CODE")
	private String countryCode;
	
	@Column(name = "CURRENCY_SYMBOL")
	private String currencySymbol;
	
	@Column(name = "CURRENCY_PREFIXED") // TODO ?
	private String currencyFixed;
	
	@Column(name = "DECIMAL_STYLE")
	private String decimalStyle;
	
	@Column(name = "DATE_STYLE_ID")
	private String dataStyleID;
	
	@Column(name = "TIME_STYLE_ID")
	private String timeStyleID;
	
	@Column(name = "COUNTRY_LANGUAGE_ID")
	private Integer countryLanguageID;
	
	@Column(name = "TIME_ZONE_CODE_ID")
	private Integer timeZoneCodeID;
	
	@Column(name = "STB_ENCODING")
	private String stbEncoding;
	
	@Column(name = "PC_ENCODING")
	private String pcEncoding;
	
	@Column(name = "DB_ENCODING")
	private String dbEncoding;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "CHANNEL_LINE_UP_ID")
	private Integer channelLineUpID;
	
	@Column(name = "DEFAULT_CHANNEL_ID")
	private Integer defaultChannelID;
	
	@Column(name = "PRICING_ID")
	private Integer pricingID;
	
	@Column(name = "LANGUAGE_CODE_ID") // TODO DUPLICATE ABOVE
	private Integer languageCodeID;
	
	@Column(name = "BOOT_STREAM_ID")
	private Integer bootStreamID;
	
	@Column(name = "HOSPITOLITY")
	private String hospitolity;
	
	@Column(name = "HOSPITOLITY_VENDOR_ID")
	private Integer hospitolityVendorID;
	
	@Column(name = "HOSPITOLITY_VENDOR_PORT")
	private String hospitolityVendorPort;
	
	
}
