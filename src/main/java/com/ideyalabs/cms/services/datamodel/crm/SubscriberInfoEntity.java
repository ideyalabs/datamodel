/**
 * Babu Gali May 16, 2020 8:27:24 PM
 */
package com.ideyalabs.cms.services.datamodel.crm;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;
import com.ideyalabs.cms.services.datamodel.common.CardTypeEntity;
import com.ideyalabs.cms.services.datamodel.common.PaymentTypeEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "subscriber_info")
@SequenceGenerator(name = "subscriber_info_seq_gen", sequenceName = "subscriber_info_seq", initialValue = 1)
public class SubscriberInfoEntity extends AbstractAuditEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7159625554913938215L;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "subscriber_info_seq_gen", strategy = GenerationType.SEQUENCE)
	private Long ID;
	
	@OneToOne
	@JoinColumn(name = "SUBSCRIBER_ID")
	private SubscriberEntity subscriber;
	
	@Column(name = "ADDRESS_ONE") // TODO PRIMARY
	private String addressOne;
	
	@Column(name = "ADDRESS_TWO")
	private String addressTwo;
	
	@Column(name = "ADDRESS_THREE")
	private String addressThree;
	
	@Column(name = "CITY")
	private Integer city;
	
	@Column(name = "SATE")
	private Integer sate;
	
	@Column(name = "ZIP_CODE")
	private String zipCode;
	
	@Column(name = "EAS_LOCATION_CODE")
	private String easLocationCode;
	
	@Column(name = "WORK_PHONE")
	private String workPhone;
	
	@Column(name = "HOME_PHONE")
	private Integer homePhone;
	
	@Column(name = "FAX")
	private String fax;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Column(name = "NOTE")
	private String note;
	
	@Column(name = "CREDIT_RATING")
	private String creditRating;
	
	@Column(name = "OVERRIDE_CREDIT_RATING")
	private String overrideCreditRating;
	
	@Column(name = "CREDIT_APPROVE_BY") // TODO USER
	private String creditApproveBy;
	
	@ManyToOne
	@JoinColumn(name = "PAYMENT_TYPE")
	private PaymentTypeEntity payentMethod;
	
	@ManyToOne
	@JoinColumn(name = "CREDIT_CARD_TYPE")
	private CardTypeEntity cardType;
	
	@Column(name = "CREDIT_CARD_NUMBER")
	private String creditCardNumber;
	
	@Column(name = "CREDIT_CARD_EXPIRE_DATE")
	private Date creditCardExpiryDate; // TODO DEBIT card type?
	
	@Column(name = "CARD_HOLD_NAME")
	private String cardHolderName;
	
	@Column(name = "STATUS")
	private Boolean status;
	
	@Column(name = "MOBILE_PHONE") // TODO REQURIED HERE?
	private Integer mobilePhone;
	
	@Column(name = "BANK_ID") // TODO bank table?
	private Integer bankID;
	
	@Column(name = "BANK_ACCOUNT")
	private String bankAcount;
	
	@Column(name = "BANK_OWNER") // TODO ?
	private String bankOwner;
	
}
