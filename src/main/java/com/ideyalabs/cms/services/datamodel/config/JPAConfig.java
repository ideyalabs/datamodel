package com.ideyalabs.cms.services.datamodel.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * May 11, 2020
 * 
 * @author Babu Gali
 *
 */
@Configuration
@EnableJpaRepositories(basePackages = { "com.ideyalabs.cms.services.datamodel.repo" })
@EntityScan(basePackages = { "com.ideyalabs.cms.services.datamodel" })
@EnableTransactionManagement
public class JPAConfig {

}
