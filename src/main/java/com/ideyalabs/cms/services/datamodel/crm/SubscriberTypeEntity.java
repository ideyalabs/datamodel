/**
 * Babu Gali
 *
 * May 22, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.crm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "user_type")
@SequenceGenerator(name = "user_type_seq_gen", sequenceName = "user_type_seq", initialValue = 1)
public class SubscriberTypeEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	/*
	 * 1 => Basic users (with login & registration)
	 * 
	 * 2 => Pre-paid cards (with login page)
	 * 
	 * 3 => Pre-authorised hardwares (Auto login with UID only)
	 */

	@Id
	@GeneratedValue(generator = "user_type_gen", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	@Column(name = "user_type")
	private String userType;

	@Column(name = "description")
	private String description;

	@Column(name = "user_type_code")
	private Integer userTypeCode;

}
