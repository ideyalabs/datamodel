/**
 * Babu Gali
 *
 * May 22, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "USER_ADDRESS")
@SequenceGenerator(name = "USER_ADDRESS_SEQ_GEN", sequenceName = "USER_ADDRESS_SEQ", initialValue = 1)
public class UserAddressEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "USER_ADDRESS_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "ADDRESS_ID")
	private AddressDetailsEntity address;

	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private UserEntity user;

	@ManyToOne
	@JoinColumn(name = "ADDRESS_TYPE_ID")
	private AddressTypeEntity addressType;

}
