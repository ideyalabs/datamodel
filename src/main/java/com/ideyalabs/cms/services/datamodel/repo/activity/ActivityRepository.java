package com.ideyalabs.cms.services.datamodel.repo.activity;

import com.ideyalabs.cms.services.datamodel.common.ActivityEntity;
import com.ideyalabs.cms.services.datamodel.repo.TableRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface ActivityRepository extends TableRepository<ActivityEntity, Long>, JpaSpecificationExecutor<ActivityEntity> {

    @Query("update #{#entityName} e set e.deleted=true where e.id=:id")
    @Modifying
    void softDeleteById(Long id);

}
