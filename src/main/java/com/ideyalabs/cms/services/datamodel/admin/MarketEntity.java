/**
 * Babu Gali
 *
 * May 22, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.admin;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "MARKET")
@SequenceGenerator(name = "MARKET_SEQ_GEN", sequenceName = "MARKET_SEQ", initialValue = 1)
public class MarketEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "MARKET_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID" )
	private Long id;

	@Column( name = "TITLE" )
	private String title;
	
	@Column( name = "CURRENCY" )
	private String currency;
	
	@Column( name = "TAX_INCLUDED_PRICE" )
	private boolean taxInPrice;
	
	@Column( name = "STATUS" )
	private boolean status;
	
	//@OneToMany
	//@JoinColumn (name ="COUNTRY_ID" )
	//private Set<CountryEntity> country;
	
	//@Column(name = "LANGUAGE" )
	//private String language;
}
