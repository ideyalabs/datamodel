
package com.ideyalabs.cms.services.datamodel.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Babu Gali
 *
 * May 23, 2020 11:03:27 AM
 * 
 * 
 */

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "DATE_STYLE")
@SequenceGenerator(name = "DATE_STYLE_SEQ_GEN", sequenceName = "DATE_STYLE_SEQ", initialValue = 1)
public class DateStyleEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "DATE_STYLE_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "CODE")
	private String code;

}
