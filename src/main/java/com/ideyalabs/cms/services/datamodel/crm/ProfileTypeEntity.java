/**
 * Babu Gali
 *
 * May 17, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.crm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "profile_type")
@SequenceGenerator(name = "profile_type_seq_gen", sequenceName = "profile_type_seq", initialValue = 1)
public class ProfileTypeEntity extends AbstractAuditEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2346189659484385017L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "profile_type_seq_gen", strategy = GenerationType.SEQUENCE)
	private Long ID;

	@Column(name = "PROFILE_TYPE")
	private Integer profileType; // VOILENT_RATING,TV_RATING,DIALOGUE_RATING,SEX_RATING,FV_RATING,CREDIT_RATING

	@Column(name = "DESCRIPTION")
	private String description;
}
