package com.ideyalabs.cms.services.datamodel.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *  @Author: Srini
 *  June 10, 2020
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ACTIVITY")
@SequenceGenerator(name = "ACTIVITY_SEQ_GEN", sequenceName = "ACTIVITY_SEQ", initialValue = 1)
public class ActivityEntity extends AbstractAuditEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "ACTIVITY_SEQ_GEN", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @Column(name = "EVENT_TYPE")
    private String eventType;

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "TENANT_ID")
    private String tenantId;

    @Column(name = "EVENT_SUB_TYPE")
    private String eventSubType;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "SERVICE_TYPE")
    private String serviceType;

    @Column(name = "EVENT_DATA")
    private String eventData;
}
