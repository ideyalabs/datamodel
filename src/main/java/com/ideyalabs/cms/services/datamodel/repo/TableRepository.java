package com.ideyalabs.cms.services.datamodel.repo;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * May 11, 2020
 * 
 * @author Babu G
 *
 * @param <T>
 * @param <ID>
 */
@NoRepositoryBean
public interface TableRepository<T, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

}