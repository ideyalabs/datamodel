package com.ideyalabs.cms.services.datamodel.crm;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;
import com.ideyalabs.cms.services.datamodel.common.AccountTypeEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author Babu Gali
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "subscriber")
@SequenceGenerator(name = "subscriber_seq_gen", sequenceName = "subscriber_seq", initialValue = 1)
public class SubscriberEntity extends AbstractAuditEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5438643976409380698L;

	@Id
	@GeneratedValue(generator = "subscriber_seq_gen", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	@Column(name = "LAST_NAME")
	private String lastName;

	@Column(name = "MIDDLE_NAME")
	private String middleName;

	@Column(name = "FIRST_NAME")
	private String firstName;

	@Column(name = "SS_NUMBER")
	private String ssNumber;

	@Column(name = "LOGIN_REQUIRED")
	private Boolean isLoginrequired;

	@Column(name = "STATUS")
	private Boolean status;

	@Column(name = "PIN")
	private String pin;

	@Column(name = "ENTITY_ID")
	private Integer entryId;

	@Column(name = "GEO_LOCATION_ID")
	private Integer geoLocationId;

	@Column(name = "DISPLAY_TIMEOUT") // TODO is it float vlue
	private Integer displayTimeOut;

	@Column(name = "PARENTAL_CONTROL")
	private Boolean isParentalControlEnabled; // TODO char

	@Column(name = "HAS_SETTINGS")
	private String settings;

	@Column(name = "CHANNEL_BLOCKING")
	private Boolean isChanelBlockingEnabled; // TODO char?

	@Column(name = "AUTO_PROVISION_COUNT")
	private Integer autoProvisionCount; // TODO decimal? and is it for tv's

	@Column(name = "AUTO_PROVISION_COUNT_MOBILE")
	private Integer mobileAutoProvisionCount;

	@Column(name = "AUTO_PROV_COUNT_STATIONARY") // TODO prov? means provision
	private Integer autoProvisionCountStationary;

	@Column(name = "EXTERNAL_SUBSCRIBER_ID")
	private String externalSubcriberID;

	@ManyToOne
	@JoinColumn(name = "REGION_ID")
	private RegionEntity region;

	@Column(name = "BLOCKED_CHANNEL_LIST") // TODO
	private String blockedChanels;

	@ManyToOne
	@JoinColumn(name = "NRTC_ACCOUNT_TYPE_ID")
	private AccountTypeEntity accountType;

	@Column(name = "FCO_DROPS")
	private Integer fcoDrops;

	@Column(name = "NPVR_LIMIT")
	private Integer npvrLimit;

	@Column(name = "PAUSELIVETV_LIMIT")
	private Integer pauseLiveTVLimit;

	@Column(name = "FAVORITES_ENABLED")
	private Boolean isFavoritesEnabled;

	@Column(name = "MULTICASTENABLED")
	private Boolean isMultiCastEnabled;

	@Column(name = "MULTICAST_TUNEIN")
	private Boolean isMulticastTuneInEnabled;

	@Column(name = "ENABLE_STREAM_MANAGEMENT")
	private Boolean isStreamManagementEnabled;

	@Column(name = "TOTAL_ATTAINABLE_BANDWIDTH")
	private Integer totalAttainableBandwidth;

	@Column(name = "QAM_ENABLED")
	private Boolean isQAMEnabled;

	// New Fields
	/*
	 * subscriptionPrice;
	 * 
	 * trial_period_days billing_period countries blocking device blocking
	 * concurrent devices
	 */
	
}
