package com.ideyalabs.cms.services.datamodel.specification;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import com.ideyalabs.cms.services.datamodel.constants.Constants;

/**
 * May 10, 2020
 * 
 * Babu Gali
 */

public class SpecificationBuilder<T> implements Specification<T> {

	private List<SpecificationFields> paramsList = null;

	private static final long serialVersionUID = 2528519319126231191L;

	/**
	 * @param paramsList
	 * @throws Exception
	 */
	public SpecificationBuilder(List<SpecificationFields> paramsList) throws Exception {

		super();
		this.paramsList = paramsList;
		validateSpecificationParams(paramsList);
	}

	/**
	 * @param paramsList
	 * @throws Exception
	 */
	private void validateSpecificationParams(List<SpecificationFields> paramsList) throws Exception {

		if (paramsList.isEmpty()) {
			throw new Exception("invlid specification");
		}

		// TODO: add more validations on paramsList
	}

	/**
	 * @param sortBy
	 * @param sortDirection
	 * @return
	 */
	public static Sort sortBy(String sortBy, String sortDirection) {
		if (sortBy == null)
			sortBy = "id";
		if(sortDirection == null){
			sortDirection = "desc";
		}
		return sortDirection.equalsIgnoreCase("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending();
	}

	/**
	 * @param pageIndex
	 * @param pageSize
	 * @param sortBy
	 * @param sortDirection
	 * @return
	 */
	public static Pageable constructPageSpecification(int pageIndex, int pageSize, String sortBy,
			String sortDirection) {
		return PageRequest.of(pageIndex, pageSize, sortBy(sortBy, sortDirection));
	}

	/**
	 *
	 */
	//@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		Predicate predicate = criteriaBuilder.conjunction();

		for (SpecificationFields specificationFields : paramsList) {
			if (specificationFields.getConditionType().equalsIgnoreCase(Constants.EQUALS)) {

				predicate = criteriaBuilder.and(predicate,
						criteriaBuilder.equal(
								specificationFields.getJoinCoulnName() == null
										? root.get(specificationFields.getColumName())
										: root.join(specificationFields.getJoinCoulnName(), JoinType.INNER)
												.get(specificationFields.getColumName()),
								specificationFields.getValue()));
			} else if (specificationFields.getConditionType().equalsIgnoreCase(Constants.ISNULL)) {
				predicate = criteriaBuilder.and(predicate,
						criteriaBuilder.isNull(root.get(specificationFields.getColumName())));

			}
		}
		return predicate;
	}

}
