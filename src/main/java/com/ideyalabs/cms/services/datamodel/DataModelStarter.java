package com.ideyalabs.cms.services.datamodel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * May 11, 2020
 * 
 * @author Babu G
 *
 */
@SpringBootApplication
public class DataModelStarter {
	public static void main(String[] args) {
		SpringApplication.run(DataModelStarter.class, args);
	}
}
