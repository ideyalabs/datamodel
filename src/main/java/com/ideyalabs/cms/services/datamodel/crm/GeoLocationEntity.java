/**
 * Babu Gali
 *
 * May 16, 2020 8:50:28 PM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.crm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.common.LocationEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "geo_location")
@SequenceGenerator(name = "geo_location_seq_gen", sequenceName = "geo_location_seq", initialValue = 1)
public class GeoLocationEntity {

	@Id
	@GeneratedValue(generator = "geo_location_seq_gen", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long ID;

	@Column(name = "NAME")
	private String name;

	@Column(name = "DESCRIPTION")
	private String description;

	/*
	 * @Column(name = "ADDRESS_ONE") private String addressOne;
	 * 
	 * @Column(name = "ADDRESS_TWO") private String addressTwo;
	 */
	@ManyToOne
	@JoinColumn(name = "CITY_ID") // TODO can we have country,state
	private LocationEntity city;

	@ManyToOne
	@JoinColumn(name = "STATE_ID")
	private LocationEntity state;

	@ManyToOne
	@JoinColumn(name = "COUNTY")
	private LocationEntity country;

	@Column(name = "STATUS")
	private Boolean status;
}
