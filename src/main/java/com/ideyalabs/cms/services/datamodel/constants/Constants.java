/**
 * May 15, 2020
 * Babu Gali
 */
package com.ideyalabs.cms.services.datamodel.constants;

/**
 * @author Babu Gali
 *
 */
public class Constants {
	
	public static final String EQUALS = "equals";
	public static final String ISNULL = null;

}
