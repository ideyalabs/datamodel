/**
 * Babu Gali
 *
 * May 22, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.admin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;
import com.ideyalabs.cms.services.datamodel.common.RoleEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ADMIN_USER")
@SequenceGenerator(name = "ADMIN_USER_SEQ_GEN", sequenceName = "ADMIN_USER_SEQ", initialValue = 1)
public class AdminUserEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "ADMIN_USER_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID" )
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "EMAIL_ID")
	private String emailId;
	
	@Column(name = "LOGIN_ID")
	private String loginID;

	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private RoleEntity role;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@Column(name = "CONFIRM_PASSWORD")
	private String confirmPassword;

	@Column(name = "LAST_ACCESSED")
	private Date lastAccessed;
	
	@Column(name = "STATUS")
	private Boolean status;

	
}
