package com.ideyalabs.cms.services.datamodel.crm;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;
import com.ideyalabs.cms.services.datamodel.common.LanguageEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * May 16, 2020 Babu Gali
 */

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "subscriber_profile")
@SequenceGenerator(name = "subscriber_profile_seq_gen", sequenceName = "subscriber_profile_seq", initialValue = 1)
public class SubscriberProfileEntity extends AbstractAuditEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4220804342487413512L;

	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "subscriber_profile_seq_gen", strategy = GenerationType.SEQUENCE)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "SUBSCRIBER_ID")
	private SubscriberEntity subscriber;

	@Column(name = "PROFILE_USER_NAME")
	private String profileUserName;

	@ManyToOne
	@JoinColumn(name = "PROFILE_TYPE")
	private ProfileTypeEntity profileType;

	@Column(name = "LOGIN_ID")
	private String loginID;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "MPAA_ACCESS_LEVEL")
	private Integer mpaaAccessLevel;

	@Column(name = "EMAIL_ENABLE")
	private Boolean isEmailEnable; //TODO : ?

	@Column(name = "RECORD_ENABLE")
	private Boolean isRecordEnable;

	@Column(name = "INTERNET_ENABLE")
	private Boolean isInternateEnable;

	@Column(name = "LAST_ACCESS_CHANNEL_ID")
	private Integer lastAccessChannelId;

	@Column(name = "SHOW_ALL_CHANNEL")
	private Boolean isShowAllChannelEnabled;

	@Column(name = "RECORD_PRIVATE")
	private String recordPrivate;

	@Column(name = "RECORD_EPISODIC")
	private String recordEpisodic;

	@Column(name = "RECORD_ALLOWANCE")
	private Double recordAllowance;

	@Column(name = "SPENDING_ALLOWANCE")
	private Double spendingAllowance;

	@Column(name = "EFFECTIVE_DATE")
	private Date effectiveDate;

	@Column(name = "EXPIRE_DATAE")
	private Date expireDate;

	@Column(name = "STATUS")
	private Boolean status;

	@ManyToOne
	@JoinColumn(name = "REGION_ID") // TODO DUPLICATE
	private RegionEntity region;

	@Column(name = "PIN_REQUIRED")
	private Boolean isPinRequired;

	@Column(name = "STB_PREFERENCE")
	private Integer sTBPreference;

	@ManyToOne
	@JoinColumn(name = "TV_RATING_ID")
	private RatingTypeEntity tvRating;
	
	@ManyToOne
	@JoinColumn(name = "SEX_RATING")
	private RatingTypeEntity sexRating;
	
	@ManyToOne
	@JoinColumn(name = "VOILENT_RATING")
	private RatingTypeEntity voilentRating;
	
	@ManyToOne
	@JoinColumn(name = "DIALOGUE_RATING")
	private RatingTypeEntity dialogueRating;
	
	@ManyToOne
	@JoinColumn(name = "FV_RATING_ID")
	private RatingTypeEntity fvRating;

	@Column(name = "REMINDER_PERIOD")
	private Integer reminderPeriod;

	@Column(name = "PPV_AUTO_SET_REMINDER")
	private Integer ppvAutoSetReminder;

	@Column(name = "EPG_FAVORITIES")
	private String epgFavorities;

	@Column(name = "BLOCK_UNREATED")
	private Boolean isBlockUnreatedEnabled;

	@Column(name = "SHOW_BLOCKED_INFO")
	private Boolean isBlockedInfoEnabled;

	@Column(name = "UNLOCK_TIMEOUT")
	private Integer unclokTimeOut;

	@Column(name = "LANGUAGE_CODE_ID")
	private Integer languageCodeID;

	@Column(name = "BOOT_STREAM_ID")
	private Integer bootStreamID;

	@Column(name = "LANGUAGE") // TODO duplicate above
	private Integer language;

	@Column(name = "SKIN")
	private String skin;

	@Column(name = "PIN")
	private String pin;

	@Column(name = "CALLER_ID")
	private Integer callerID;

	@ManyToOne
	@JoinColumn(name = "PRIMARY_AUDIO_LANGUAGE")
	private LanguageEntity primaryAudioLanguageID;

	@ManyToOne
	@JoinColumn(name = "SECONDARY_AUDIO_LANGUAGE")
	private LanguageEntity secondaryAudioLanguageID;

	@ManyToOne
	@JoinColumn(name = "PRIMARY_SUBTITLE_LANGUAGE")
	private LanguageEntity primarySubtittleLanguageId;

	@ManyToOne
	@JoinColumn(name = "SECONDARY_SUBTITLE_LANGUAGE")
	private LanguageEntity seconadrySubtittledLanguage;

	@Column(name = "CLOSED_CAPTIONS")
	private String closedCaptions;

	@Column(name = "DESCRIPTIVE_VIDEO")
	private String descriptiveVideo;

	@Column(name = "IMAGE_ID_FOCUSED")
	private String imageIdFocused;

	@Column(name = "IMAGE_ID_UNFOCUSED")
	private String imageIdUnFocused;

	@Column(name = "NOTIFICATION_TIMEOUT") // TODO APP LEVEL
	private Integer notificationTimeout;

	@Column(name = "ACCOUNT_LOGIN_REQUIRED")
	private Boolean accountLoginrequired;

	@Column(name = "IDP_TIMESTAMP")
	private Date idpTimestamp;

}
