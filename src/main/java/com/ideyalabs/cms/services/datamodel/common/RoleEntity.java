
package com.ideyalabs.cms.services.datamodel.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * Babu Gali
 *
 * May 23, 2020 11:03:27 AM
 * 
 * 
 */

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ROLE")
@SequenceGenerator(name = "ROLE_SEQ_GEN", sequenceName = "ROLE_SEQ", initialValue = 1)
public class RoleEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "ROLE_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	@Column(name = "ROLE_TYPE")
	private String roleType; //ADMIN,USER

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "ROLE_CODE")
	private Integer roleCode;
	
	@Column (name ="ALLOW_ALL_PERMISSIONS" )
	private boolean allowAllPermissions;

}
