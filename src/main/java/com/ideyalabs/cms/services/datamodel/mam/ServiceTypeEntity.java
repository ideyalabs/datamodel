
package com.ideyalabs.cms.services.datamodel.mam;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * May 16, 2020 Babu Gali
 */

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "SERVICE_TYPE")
@SequenceGenerator(name = "SERVICE_TYPE_SEQ_GEN", sequenceName = "SERVICE_TYPE_SEQ", initialValue = 1)
public class ServiceTypeEntity extends AbstractAuditEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2346189659484385017L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "SERVICE_TYPE_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	private Long ID;

	@Column(name = "SERVICE_TYPE")
	private Integer serviceType;

	@Column(name = "SERVICE_CODE")
	private Integer serviceCode;

	@Column(name = "DESCRIPTION")
	private String description;

}