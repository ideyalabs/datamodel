package com.ideyalabs.cms.services.datamodel.repo.crm;

import com.ideyalabs.cms.services.datamodel.crm.SubscriberProfileEntity;
import com.ideyalabs.cms.services.datamodel.repo.TableRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface SubscriberProfileRepository extends TableRepository<SubscriberProfileEntity, Long>, JpaSpecificationExecutor<SubscriberProfileEntity> {

    @Query("SELECT t FROM #{#entityName} t WHERE t.loginID = ?1 AND t.password = ?2")
    SubscriberProfileEntity getUser(String loginId, String password);

    SubscriberProfileEntity findOneByLoginID(String loginId);
}
