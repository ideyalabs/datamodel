package com.ideyalabs.cms.services.datamodel.repo.admin;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ideyalabs.cms.services.datamodel.admin.TaxEntity;
import com.ideyalabs.cms.services.datamodel.repo.TableRepository;

/**
 * 
 * @author sanjeev P
 *
 */
public interface TaxRepository extends TableRepository<TaxEntity, Long>, JpaSpecificationExecutor<TaxEntity> {

	/**
	 * @param id
	 */
	@Query("update #{#entityName} e set e.deleted=true where e.id=:id")
	@Modifying
	void softDeleteById(Long id);
}
