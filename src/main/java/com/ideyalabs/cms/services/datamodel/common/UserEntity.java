/**
 * Babu Gali
 *
 * May 22, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.common;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "USER")
@SequenceGenerator(name = "USER_SEQ_GEN", sequenceName = "USER_SEQ", initialValue = 1)
public class UserEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "USER_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	@Column(name = "FIST_NAME")
	private String firstName;

	@Column(name = "LAST_NAME")
	private String lastName;
	
	@Column(name = "LOGIN_ID")
	private String loginID;

	@Column(name = "PASSWORD")
	private String password;

	
	@Column(name = "STATUS")
	private Boolean status;

	@Column(name = "LAST_ACCESSED")
	private Date lastAccessed;

	@Column(name = "EXPIRE_DATE")
	private Date expireDate;

	@ManyToOne
	@JoinColumn(name = "ROLE_ID")
	private RoleEntity role;
	
	@OneToOne
	@JoinColumn(name = "CONTACT_ID")
	private UserContactEntity userContact;

}
