
package com.ideyalabs.cms.services.datamodel.crm;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * May 16, 2020 Babu Gali
 */

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "rating")
@SequenceGenerator(name = "rating_seq_gen", sequenceName = "rating_seq", initialValue = 1)
public class RatingTypeEntity extends AbstractAuditEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2346189659484385017L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "rating_seq_gen", strategy = GenerationType.SEQUENCE)
	private Long ID;

	@Column(name = "RATING_TYPE")
	private Integer assetRating; // VOILENT_RATING,TV_RATING,DIALOGUE_RATING,SEX_RATING,FV_RATING,CREDIT_RATING

	@Column(name = "DESCRIPTION")
	private String description;

}