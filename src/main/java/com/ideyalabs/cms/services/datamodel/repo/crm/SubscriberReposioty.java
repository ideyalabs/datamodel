package com.ideyalabs.cms.services.datamodel.repo.crm;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ideyalabs.cms.services.datamodel.crm.SubscriberEntity;
import com.ideyalabs.cms.services.datamodel.repo.TableRepository;

/**
 * May 11, 2020
 * 
 * @author Babu Gali
 *
 */
public interface SubscriberReposioty
		extends TableRepository<SubscriberEntity, Long>, JpaSpecificationExecutor<SubscriberEntity> {

	/**
	 * @param id
	 */
	@Query("update #{#entityName} e set e.deleted=true where e.id=:id")
	@Modifying
	void softDeleteById(Long id);

}
