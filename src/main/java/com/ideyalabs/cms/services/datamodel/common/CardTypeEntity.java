/**
 * Babu Gali
 *
 * May 22, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "CARD_TYPE")
@SequenceGenerator(name = "CARD_TYPE_SEQ_GEN", sequenceName = "CARD_TYPE_SEQ", initialValue = 1)
public class CardTypeEntity extends AbstractAuditEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1160607415371917113L;

	@Id
	@GeneratedValue(generator = "CARD_TYPE_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	@Column(name = "CARD_TYPE")
	private String cardType;

	@Column(name = "DESCRIPTION")
	private String description;

}
