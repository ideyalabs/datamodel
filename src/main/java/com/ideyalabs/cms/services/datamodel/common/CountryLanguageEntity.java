/**
 * Babu Gali
 *
 * May 22, 2020 12:03:27 AM
 * 
 * 
 */
package com.ideyalabs.cms.services.datamodel.common;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 *
 */
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "COUNTRY_LANGUAGE")
@SequenceGenerator(name = "COUNTRY_LANGUAGE_SEQ_GEN", sequenceName = "COUNTRY_LANGUAGE_SEQ", initialValue = 1)
public class CountryLanguageEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(generator = "COUNTRY_LANGUAGE_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	
	@Column(name = "COUNTRY_ID")
	private LocationEntity country;

	@Column(name = "LANGUAGE_ID")
	private LanguageEntity language;

}
