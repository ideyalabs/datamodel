
package com.ideyalabs.cms.services.datamodel.mam;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * May 16, 2020 Babu Gali
 */

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "CHANNEL_TYPE")
@SequenceGenerator(name = "CHANNEL_TYPE_SEQ_GEN", sequenceName = "CHANNEL_TYPE_SEQ", initialValue = 1)
public class ChannelTypeEntity extends AbstractAuditEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2346189659484385017L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(generator = "CHANNEL_TYPE_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	private Long ID;

	@Column(name = "CHANNEL_TYPE")
	private Integer channelType; 

	@Column(name = "DESCRIPTION")
	private String description;

}