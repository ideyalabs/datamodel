package com.ideyalabs.cms.services.datamodel.repo.admin;

import com.ideyalabs.cms.services.datamodel.common.UserEntity;
import com.ideyalabs.cms.services.datamodel.repo.TableRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface UserEntityRepository extends TableRepository<UserEntity, Long>, JpaSpecificationExecutor<UserEntity> {

    @Query("SELECT t FROM UserEntity t WHERE t.loginID = ?1 AND t.password = ?2")
    UserEntity getUser(String loginId, String password);
    UserEntity findOneByLoginID(String loginId);
}