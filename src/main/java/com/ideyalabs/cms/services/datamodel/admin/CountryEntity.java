/**
 * 
 */
package com.ideyalabs.cms.services.datamodel.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.ideyalabs.cms.services.datamodel.audit.AbstractAuditEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "COUNTRY")
@SequenceGenerator(name = "COUNTRY_SEQ_GEN", sequenceName = "COUNTRY_SEQ", initialValue = 1)
public class CountryEntity extends AbstractAuditEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "COUNTRY_SEQ_GEN", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID" )
	private Long id;

	@Column( name = "NAME" )
	private String name;
	
	@Column( name = "COUNTRY_CODE")
	private String countryCode;
	
	@Column( name = "CODE_NUMBER")
	private Long codeNumber;

	@Column(name = "COORDINATES")
	private String coordinates;
	
	@Column ( name = "STATUS")
	private boolean status;
	
}
